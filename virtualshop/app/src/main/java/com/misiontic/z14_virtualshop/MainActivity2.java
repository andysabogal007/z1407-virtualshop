package com.misiontic.z14_virtualshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toast.makeText(this, "Se inicio 2 Vista", Toast.LENGTH_LONG).show();
    }

    public void Atras(View view){
        Intent cambiar = new Intent(this, MainActivity.class);
        startActivity(cambiar);
    }

    public void Siguiente(View view){
        Intent cambiar = new Intent(this, MainActivity3.class);
        startActivity(cambiar);
    }
}