package com.misiontic.z14_virtualshop;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class NuevaActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva);
        Button btnRegistro = findViewById(R.id.ir_registro_btn);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),LoginActivity4.class);
                startActivity(i);
            }
        });
        Button btnIngreso = findViewById(R.id.add_btnCrearcuenta);
        btnIngreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cre = new Intent(v.getContext(),ProductosActivity4.class);
                startActivity(cre);
            }
        });



        Button btninins = findViewById(R.id.nueva_btninstagram);
        btninins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                mensaje.setTitle("confirmación de ingreso");
                mensaje.setMessage("Confirme el ingreso al sitio por la cuenta de Instagram")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Intent regg = new Intent(v.getContext(), ProductosActivity4.class);
                                startActivity(regg);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .setCancelable(false);
                mensaje.show();

            }
        });


        Button btninsgo = findViewById(R.id.nueva_btnGoogle);
        btninsgo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                mensaje.setTitle("confirmación de ingreso");
                mensaje.setMessage("Confirme el ingreso al sitio por la cuenta de Google")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Intent regg = new Intent(v.getContext(), ProductosActivity4.class);
                                startActivity(regg);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .setCancelable(false);
                mensaje.show();

            }
        });

        Button btninfac = findViewById(R.id.nueva_btnFacebook);
        btninfac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                mensaje.setTitle("confirmación de ingreso");
                mensaje.setMessage("Confirme el ingreso al sitio por la cuenta de Facebook")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Intent regg = new Intent(v.getContext(), ProductosActivity4.class);
                                startActivity(regg);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .setCancelable(false);
                mensaje.show();

            }
        });

    }
}