package com.misiontic.z14_virtualshop;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button A;
        Button B;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(MainActivity.this, "Se inicio la Aplicacion", Toast.LENGTH_SHORT).show();
        A = findViewById(R.id.Mostrar_Alerta);
        B = findViewById(R.id.Siguiente);


        A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alerta = new AlertDialog.Builder(MainActivity.this);
                alerta.setMessage("Entendido el tema de las alertas?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .setCancelable(false);

                AlertDialog titulo = alerta.create();
                titulo.setTitle("Hola mundo");
                titulo.show();

            }
        });

        B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });

        /*A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DbHelper mydb = new DbHelper(MainActivity.this);
                SQLiteDatabase db = mydb.getWritableDatabase();
                if(db != null){
                    Toast.makeText(MainActivity.this, "Se Creo", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "No se Creo", Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }
}