package com.misiontic.z14_virtualshop;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.fondoaux);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login4);
        Button btnInscribir = findViewById(R.id.login_btnInscribirse);
        btnInscribir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ins = new Intent(v.getContext(),NuevaActivity.class);
                startActivity(ins);
            }
        });
        Button btninscr = findViewById(R.id.login_btnCRegistro);
        btninscr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText usuario = findViewById(R.id.login_layoutNickname);
                EditText clave = findViewById(R.id.login_layoutContraseña);


                String valorUsuario = usuario.getText().toString();
                String valorClave = clave.getText().toString();

                if (!valorUsuario.isEmpty() && !valorClave.isEmpty()){
                    AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                    mensaje.setTitle("confirmación de ingreso");
                    mensaje.setMessage("Confirme el ingreso al sitio")
                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            /*finish();*/
                            Intent regi = new Intent(v.getContext(), ProductosActivity4.class);
                            regi.putExtra("usuario", valorUsuario);
                            startActivity(regi);
                        }
                    })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            })
                            .setCancelable(false);
                    mensaje.show();

                }
                else
                {
                    Toast.makeText(v.getContext(), "Digite todos los campos requeridos", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button btninins = findViewById(R.id.login_btninstagram);
        btninins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                mensaje.setTitle("confirmación de ingreso");
                mensaje.setMessage("Confirme el ingreso al sitio por la cuenta de Instagram")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Intent regg = new Intent(v.getContext(), ProductosActivity4.class);
                                startActivity(regg);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .setCancelable(false);
                mensaje.show();

            }
        });


        Button btninsgo = findViewById(R.id.login_btnGoogle);
        btninsgo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                mensaje.setTitle("confirmación de ingreso");
                mensaje.setMessage("Confirme el ingreso al sitio por la cuenta de Google")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Intent regg = new Intent(v.getContext(), ProductosActivity4.class);
                                startActivity(regg);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .setCancelable(false);
                mensaje.show();

            }
        });

        Button btninfac = findViewById(R.id.login_btnFacebook);
        btninfac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                mensaje.setTitle("confirmación de ingreso");
                mensaje.setMessage("Confirme el ingreso al sitio por la cuenta de Facebook")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Intent regg = new Intent(v.getContext(), ProductosActivity4.class);
                                startActivity(regg);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .setCancelable(false);
                mensaje.show();

            }
        });



    }
}